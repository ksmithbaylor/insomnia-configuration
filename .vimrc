filetype plugin indent on     " required for Vundle

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

Bundle 'gmarik/vundle'

Bundle 'git://git.wincent.com/command-t.git'
Bundle 'scrooloose/nerdtree'
Bundle 'scrooloose/nerdcommenter'
Bundle 'ack.vim'
Bundle 'snipMate'
Bundle 'vim-scripts/Rainbow-Parentheses-Improved-and2'
"Bundle 'clang-complete'
"Bundle 'SingleCompile'
Bundle 'unimpaired.vim'


"much of the following was found at stevelosh.com/blog/2010/09/coming-home-to-vim/

"turn syntax highlighting on always
syntax on
color tir_black

"gets rid of unneccessary vi backwards-compatibility
set nocompatible

"tabs always expand into 4 spaces
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab

"general options to make things better
set encoding=utf-8
set scrolloff=3
set autoindent
set showmode
set showcmd
set hidden
set wildmenu
set wildmode=list:longest
set visualbell
set cursorline
set ttyfast
set ruler
set backspace=indent,eol,start
set laststatus=2
set number

"remapping leader from \ to ,
let mapleader = ","

"tame searching and moving
nnoremap / /\v
vnoremap / /\v
set ignorecase
set smartcase
set gdefault
set incsearch
set showmatch
set hlsearch
nnoremap <leader><space> :noh<cr>
nnoremap <tab> %
vnoremap <tab> %

"make vim handle long lines correctly
set wrap
set textwidth=79
set formatoptions=qrn1

"show invisible characters
set list
set listchars=tab:▸\

"crutches to force me to learn vim right, without the arrow keys
"nnoremap <up> <nop>
"nnoremap <down> <nop>
"nnoremap <left> <nop>
"nnoremap <right> <nop>
"inoremap <up> <nop>
"inoremap <down> <nop>
"inoremap <left> <nop>
"inoremap <right> <nop>
nnoremap j gj
nnoremap k gk

"remap : to ; to avoid shift all the time
nnoremap ; :

"save all files when switching focus
au FocusLost * :wa

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"leader customizations

"W strips all trailing whitespace in the current file
nnoremap <leader>W :%s/\s\+$//<cr>:let @/=''<CR>

"a is a shortcut for Ack
nnoremap <leader>a :Ack<space>

"v selects the text that was just pasted so I can perform commands on it
nnoremap <leader>v V`]

"ev opens up the vimrc file in a vertically split window
nnoremap <leader>ev <C-w><C-v><C-l>:e $MYVIMRC<cr>

"w splits the window vertically and switches to the new one
nnoremap <leader>w <C-w>v<C-w>l

"print the current document via postscript
nnoremap <silent> <leader>p :silent :hardcopy > printed.ps<CR>:.!open printed.ps && rm -f printed.ps<CR>u<CR><up>

"eliminate unnecessary <C-w> when moving between splits
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"remap jj to <ESC> in insert mode for quicker escaping
inoremap jj <ESC>

" Bubble single lines
nmap <C-Up> [e
nmap <C-Down> ]e
imap <C-Up> <ESC>[egi
imap <C-Down> <ESC>]egi
" Bubble multiple lines
vmap <C-Up> [egv
vmap <C-Down> ]egv

"fix curly braces in c++ and such
inoremap {      {}<Left>
inoremap {<CR>  {<CR>}<Esc>O
inoremap {{     {
inoremap {}     {}

"NERDTree settings
map <C-e> :NERDTreeToggle<CR>
autocmd vimenter * if !argc() | NERDTree | endif "open it if no files specified
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif
let g:NERDTreeChDirMode=2
let g:NERDTreeWinPos = "right"

