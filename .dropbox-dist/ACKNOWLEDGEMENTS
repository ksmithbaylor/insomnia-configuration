
Regarding our inclusion of FreeType:
        Portions of this software are copyright (C) 2008 The FreeType
        Project (www.freetype.org).  All rights reserved.

Regarding our inclusion of dbus-python:
        Portions copyright:
        Copyright (C) 2003-2007 Red Hat Inc. <http://www.redhat.com/>
        Copyright (C) 2003 David Zeuthen
        Copyright (C) 2004 Rob Taylor
        Copyright (C) 2005-2008 Collabora Ltd. <http://www.collabora.co.uk/>
        Copyright (C) 2004 Anders Carlsson
        Copyright (C) 2008 Openismus GmbH <http://openismus.com/>

	Permission is hereby granted, free of charge, to any person
	obtaining a copy of this software and associated documentation
	files (the "Software"), to deal in the Software without
	restriction, including without limitation the rights to use, copy,
	modify, merge, publish, distribute, sublicense, and/or sell copies
	of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

        The above copyright notice and this permission notice shall be included in all
        copies or substantial portions of the Software.

        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
        IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
        FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
        AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
        LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
        OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
        SOFTWARE.

Regarding our inclusion of POW (python openssl wrapper):
        /*****************************************************************************/
        /*                                                                           */
        /*  Copyright (c) 2001, 2002, Peter Shannon                                  */
        /*  All rights reserved.                                                     */
        /*                                                                           */
        /*  Redistribution and use in source and binary forms, with or without       */
        /*  modification, are permitted provided that the following conditions       */
        /*  are met:                                                                 */
        /*                                                                           */
        /*      * Redistributions of source code must retain the above               */
        /*        copyright notice, this list of conditions and the following        */
        /*        disclaimer.                                                        */
        /*                                                                           */
        /*      * Redistributions in binary form must reproduce the above            */
        /*        copyright notice, this list of conditions and the following        */
        /*        disclaimer in the documentation and/or other materials             */
        /*        provided with the distribution.                                    */
        /*                                                                           */
        /*      * The name of the contributors may be used to endorse or promote     */
        /*        products derived from this software without specific prior         */
        /*        written permission.                                                */
        /*                                                                           */
        /*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      */
        /*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        */
        /*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS        */
        /*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS   */
        /*  OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,          */
        /*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT         */
        /*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,    */
        /*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY    */
        /*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT      */
        /*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE    */
        /*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.     */
        /*                                                                           */
        /*****************************************************************************/

Regarding our inclusion of librsync (http://www.freedesktop.org/wiki/Software/DBusBindings):
        Portions copyright:
        Copyright (C) 1999-2002, 2004 by Martin Pool <mbp@samba.org>
        Copyright (C) 1996-1999 by Andrew Tridgell <tridge@samba.org>
        Copyright (C) 1994-2003 Free Software Foundation, Inc.
        Copyright (C) 2002 by Ben Elliston <bje@redhat.com>
        Copyright (C) 1995 Patrick Powell (papowell@astart.com)
        Copyright (C) 2002, 2003 by Donovan Baarda <abo@minkirri.apana.org.au> 
        Copyright (C) 1996 by Paul Mackerras
        Copyright (C) 1994 X Consortium

        This library is free software; you can redistribute it and/or
        modify it under the terms of the GNU Lesser General Public
        License as published by the Free Software Foundation; either
        version 2 of the License, or (at your option) any later version.

        This library is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
        Lesser General Public License for more details.

        You should have received a copy of the GNU Lesser General Public
        License along with this library; if not, write to the Free Software
        Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA

Regarding our inclusion of expat (http://expat.sourceforge.net/):
        Copyright (c) 1998, 1999, 2000 Thai Open Source Software Center Ltd
                                       and Clark Cooper
        Copyright (c) 2001, 2002, 2003, 2004, 2005, 2006 Expat maintainers.

        Permission is hereby granted, free of charge, to any person obtaining
        a copy of this software and associated documentation files (the
        "Software"), to deal in the Software without restriction, including
        without limitation the rights to use, copy, modify, merge, publish,
        distribute, sublicense, and/or sell copies of the Software, and to
        permit persons to whom the Software is furnished to do so, subject to
        the following conditions:

        The above copyright notice and this permission notice shall be included
        in all copies or substantial portions of the Software.

        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
        EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
        MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
        IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
        CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
        TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
        SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Regarding our include of openssl (http://openssl.org/):
        /* ====================================================================
         * Copyright (c) 1998-2007 The OpenSSL Project.  All rights reserved.
         *
         * Redistribution and use in source and binary forms, with or without
         * modification, are permitted provided that the following conditions
         * are met:
         *
         * 1. Redistributions of source code must retain the above copyright
         *    notice, this list of conditions and the following disclaimer. 
         *
         * 2. Redistributions in binary form must reproduce the above copyright
         *    notice, this list of conditions and the following disclaimer in
         *    the documentation and/or other materials provided with the
         *    distribution.
         *
         * 3. All advertising materials mentioning features or use of this
         *    software must display the following acknowledgment:
         *    "This product includes software developed by the OpenSSL Project
         *    for use in the OpenSSL Toolkit. (http://www.openssl.org/)"
         *
         * 4. The names "OpenSSL Toolkit" and "OpenSSL Project" must not be used to
         *    endorse or promote products derived from this software without
         *    prior written permission. For written permission, please contact
         *    openssl-core@openssl.org.
         *
         * 5. Products derived from this software may not be called "OpenSSL"
         *    nor may "OpenSSL" appear in their names without prior written
         *    permission of the OpenSSL Project.
         *
         * 6. Redistributions of any form whatsoever must retain the following
         *    acknowledgment:
         *    "This product includes software developed by the OpenSSL Project
         *    for use in the OpenSSL Toolkit (http://www.openssl.org/)"
         *
         * THIS SOFTWARE IS PROVIDED BY THE OpenSSL PROJECT ``AS IS'' AND ANY
         * EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
         * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
         * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE OpenSSL PROJECT OR
         * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
         * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
         * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
         * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
         * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
         * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
         * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
         * OF THE POSSIBILITY OF SUCH DAMAGE.
         * ====================================================================
         *
         * This product includes cryptographic software written by Eric Young
         * (eay@cryptsoft.com).  This product includes software written by Tim
         * Hudson (tjh@cryptsoft.com).
         *
         */

         Original SSLeay License
         -----------------------

        /* Copyright (C) 1995-1998 Eric Young (eay@cryptsoft.com)
         * All rights reserved.
         *
         * This package is an SSL implementation written
         * by Eric Young (eay@cryptsoft.com).
         * The implementation was written so as to conform with Netscapes SSL.
         * 
         * This library is free for commercial and non-commercial use as long as
         * the following conditions are aheared to.  The following conditions
         * apply to all code found in this distribution, be it the RC4, RSA,
         * lhash, DES, etc., code; not just the SSL code.  The SSL documentation
         * included with this distribution is covered by the same copyright terms
         * except that the holder is Tim Hudson (tjh@cryptsoft.com).
         * 
         * Copyright remains Eric Young's, and as such any Copyright notices in
         * the code are not to be removed.
         * If this package is used in a product, Eric Young should be given attribution
         * as the author of the parts of the library used.
         * This can be in the form of a textual message at program startup or
         * in documentation (online or textual) provided with the package.
         * 
         * Redistribution and use in source and binary forms, with or without
         * modification, are permitted provided that the following conditions
         * are met:
         * 1. Redistributions of source code must retain the copyright
         *    notice, this list of conditions and the following disclaimer.
         * 2. Redistributions in binary form must reproduce the above copyright
         *    notice, this list of conditions and the following disclaimer in the
         *    documentation and/or other materials provided with the distribution.
         * 3. All advertising materials mentioning features or use of this software
         *    must display the following acknowledgement:
         *    "This product includes cryptographic software written by
         *     Eric Young (eay@cryptsoft.com)"
         *    The word 'cryptographic' can be left out if the rouines from the library
         *    being used are not cryptographic related :-).
         * 4. If you include any Windows specific code (or a derivative thereof) from 
         *    the apps directory (application code) you must include an acknowledgement:
         *    "This product includes software written by Tim Hudson (tjh@cryptsoft.com)"
         * 
         * THIS SOFTWARE IS PROVIDED BY ERIC YOUNG ``AS IS'' AND
         * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
         * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
         * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
         * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
         * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
         * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
         * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
         * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
         * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
         * SUCH DAMAGE.
         * 
         * The licence and distribution terms for any publically available version or
         * derivative of this code cannot be changed.  i.e. this code cannot simply be
         * copied and put under another distribution licence
         * [including the GNU Public Licence.]
         */

Regarding our inclusion of PyCrypto (https://www.dlitz.net/software/pycrypto/):
        Copyright and licensing of the Python Cryptography Toolkit ("PyCrypto"):
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        Previously, the copyright and/or licensing status of the Python
        Cryptography Toolkit ("PyCrypto") had been somewhat ambiguous. The
        original intention of Andrew M. Kuchling and other contributors has
        been to dedicate PyCrypto to the public domain, but that intention was
        not necessarily made clear in the original disclaimer (see
        LEGAL/copy/LICENSE.orig).

        Additionally, some files within PyCrypto had specified their own
        licenses that differed from the PyCrypto license itself. For example,
        the original RIPEMD.c module simply had a copyright statement and
        warranty disclaimer, without clearly specifying any license terms.
        (An updated version on the author's website came with a license that
        contained a GPL-incompatible advertising clause.)

        To rectify this situation for PyCrypto 2.1, the following steps have
        been taken:

         1. Obtaining explicit permission from the original contributors to
            dedicate their contributions to the public domain if they have not
            already done so. (See the "LEGAL/copy/stmts" directory for
            contributors' statements.)

         2. Replacing some modules with clearly-licensed code from other
            sources (e.g. the DES and DES3 modules were replaced with new ones
            based on Tom St. Denis's public-domain LibTomCrypt library.)

         3. Replacing some modules with code written from scratch (e.g. the
            RIPEMD and Blowfish modules were re-implemented from their
            respective algorithm specifications without reference to the old
            implementations).

         4. Removing some modules altogether without replacing them.

        To the best of our knowledge, with the exceptions noted below or
        within the files themselves, the files that constitute PyCrypto are in
        the public domain. Most are distributed with the following notice:

          The contents of this file are dedicated to the public domain. To
          the extent that dedication to the public domain is not available,
          everyone is granted a worldwide, perpetual, royalty-free,
          non-exclusive license to exercise all rights associated with the
          contents of this file for any purpose whatsoever.
          No rights are reserved.

          THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
          EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
          MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
          NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
          BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
          ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
          CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
          SOFTWARE.

        Exception:

         - Portions of HMAC.py and setup.py are derived from Python 2.2, and
           are therefore Copyright (c) 2001, 2002, 2003 Python Software
           Foundation (All Rights Reserved). They are licensed by the PSF
           under the terms of the Python 2.2 license. (See the file
           LEGAL/copy/LICENSE.python-2.2 for details.)

        EXPORT RESTRICTIONS:

        Note that the export or re-export of cryptographic software and/or
        source code may be subject to regulation in your jurisdiction.

Regarding our inclusion of SocksiPy (http://socksipy.sourceforge.net/):
        Copyright 2006 Dan-Haim. All rights reserved.

        Redistribution and use in source and binary forms, with or without modification,
        are permitted provided that the following conditions are met:
        1. Redistributions of source code must retain the above copyright notice, this
           list of conditions and the following disclaimer.
        2. Redistributions in binary form must reproduce the above copyright notice,
           this list of conditions and the following disclaimer in the documentation
           and/or other materials provided with the distribution.
        3. Neither the name of Dan Haim nor the names of his contributors may be used
           to endorse or promote products derived from this software without specific
           prior written permission.

        THIS SOFTWARE IS PROVIDED BY DAN HAIM "AS IS" AND ANY EXPRESS OR IMPLIED
        WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
        MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
        EVENT SHALL DAN HAIM OR HIS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
        INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
        LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA
        OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
        LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
        OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMANGE.
